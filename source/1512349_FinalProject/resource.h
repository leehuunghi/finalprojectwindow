//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512349_FinalProject.rc
//
#define IDC_MYICON                      2
#define IDD_MY1512349_FINALPROJECT_DIALOG 102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDC_LISTVIEW1                   104
#define IDM_EXIT                        105
#define IDC_LISTVIEW2                   105
#define IDI_MY1512349_FINALPROJECT      107
#define IDC_MY1512349_FINALPROJECT      109
#define ID_NOTIFI                       110
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     129
#define IDI_ICON1                       133
#define IDI_ICON2                       134
#define IDR_MENU1                       135
#define IDD_DIALOG1                     136
#define IDD_DIALOG2                     137
#define IDC_CANCEL                      1000
#define IDC_SAVE                        1001
#define ID_VIEWNOTES                    32771
#define ID_                             32772
#define ID_EXITNOTI                     32773
#define ID_VIEWSTA                      32774
#define ID_VIEWNOTES_VIEWNOTES          32775
#define ID_VIEWNOTES_VIEWSTATISTICS     32776
#define ID_VIEWNOTES_EXIT               32777
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
