#pragma once
#include "Note.h"
#include <vector>

class CTag
{
private:
	string key;
	vector<CNote*> sNotes;
public:
	CTag();
	void setKey(string);
	void addNote(CNote*);
	string getName(){ return key; };
	int getNumberNote(){ return sNotes.size();};
	vector<CNote*> getVNote(){ return sNotes; };
	~CTag();
};

