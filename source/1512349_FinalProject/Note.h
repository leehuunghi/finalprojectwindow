#pragma once
#include <iostream>
#include <string>
using namespace std;


class CNote
{
private:
	string content;
public:
	CNote();
	CNote(string);
	void set(string content){ this->content = content; };
	string get(){ return content; };
	~CNote();
};

