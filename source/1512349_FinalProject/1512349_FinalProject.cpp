﻿// 1512349_FinalProject.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512349_FinalProject.h"
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#include <Shellapi.h>
#include <Windows.h>
#include <windowsx.h>
#include "Tag.h"
//Dùng để sử dụng hàm StrCpy, StrNCat
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

// TreeView & ListView
#include <commctrl.h>
#pragma comment(lib, "Comctl32.lib")

#include <AtlBase.h>
#include <AtlConv.h>
#define MAX_LOADSTRING 100
#define WM_CALLBACKNOTE 1997
// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
HWND hStatic, hTag, hNote, gListView1, gListView2, gContentNote;
HHOOK hHook = NULL;
HWND hWnd;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK AddNote(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK ViewNotes(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
string getTextToStr(HWND h);
vector<CTag*> sTag;
vector<CNote*> sNote;
int findTag(string tagName);

NOTIFYICONDATA nid = {};
void ShowContextMenu(HWND, POINT);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MY1512349_FINALPROJECT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1512349_FINALPROJECT));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

LRESULT CALLBACK HookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (wParam == WM_KEYUP
		&& (GetAsyncKeyState(VK_SPACE) & 0x8000)
		&& (GetAsyncKeyState(VK_CONTROL) & 0x8000))
	{
		DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, AddNote);
		UnhookWindowsHookEx(hHook);
		hHook = NULL;
		return TRUE;
	}
	return CallNextHookEx(hHook, nCode, wParam, lParam);
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1512349_FINALPROJECT));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MY1512349_FINALPROJECT);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON2));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}


//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:
	{
					  INITCOMMONCONTROLSEX icex;

					  // Ensure that the common control DLL is loaded. 
					  icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
					  icex.dwICC = ICC_LISTVIEW_CLASSES;
					  InitCommonControlsEx(&icex);

					  //xử lý notification xuống góc màn hình
					  NOTIFYICONDATA nid = { sizeof(nid) };
					  nid.cbSize = sizeof(nid);
					  nid.hWnd = hWnd;
					  nid.uFlags = NIF_ICON | NIF_TIP | NIF_GUID |NIF_MESSAGE;
					  nid.uCallbackMessage = WM_CALLBACKNOTE;

					  nid.hIcon = (HICON)LoadImage(hInst,
						  MAKEINTRESOURCE(IDI_ICON2),
						  IMAGE_ICON,
						  GetSystemMetrics(SM_CXSMICON),
						  GetSystemMetrics(SM_CYSMICON),
						  LR_DEFAULTCOLOR);
					  nid.uVersion = NOTIFYICON_VERSION_4;

					  // Show the notification.
					  Shell_NotifyIcon(NIM_ADD, &nid) ? S_OK : E_FAIL;

					  if (hHook == NULL) hHook = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)HookProc, hInst, 0);
	}
		break;
	case WM_CALLBACKNOTE:
		switch (LOWORD(lParam))
		{
		case WM_RBUTTONDOWN:
		{
							   POINT pt;
							   GetCursorPos(&pt);
							   ShowContextMenu(hWnd, pt);		
		}
			break;
		}
		break;
	case WM_NOTIFY:

		break;
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_VIEWNOTES_VIEWNOTES:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG2), hWnd, ViewNotes);
			break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;			
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		UnhookWindowsHookEx(hHook);
		hHook = NULL;
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void ShowContextMenu(HWND hwnd, POINT pt)
{
	HMENU hMenu = LoadMenu(hInst, MAKEINTRESOURCE(IDR_MENU1));
	if (hMenu)
	{
		HMENU hSubMenu = GetSubMenu(hMenu, 0);
		if (hSubMenu)
		{
			SetForegroundWindow(hwnd);

			UINT uFlags = TPM_RIGHTBUTTON;
			if (GetSystemMetrics(SM_MENUDROPALIGNMENT) != 0)
				uFlags |= TPM_RIGHTALIGN;
			else
				uFlags |= TPM_LEFTALIGN;
			TrackPopupMenuEx(hSubMenu, uFlags, pt.x, pt.y, hwnd, NULL);
		}
		DestroyMenu(hMenu);
	}
}

INT_PTR CALLBACK AddNote(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	string strTag, contentNote;
	CNote* note;
	CTag tag;
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(lf.lfHeight, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, TEXT("Segoe UI"));
	switch (message)
	{
	case WM_INITDIALOG:
		hStatic = CreateWindowEx(0, L"STATIC", L"Tag:",
			WS_CHILD | WS_VISIBLE, 0, 0, 450, 20,
			hDlg, NULL, NULL, NULL);
		SetWindowFont(hStatic, hFont, 0);
		hTag = CreateWindowEx(0, L"EDIT", L"",
			WS_CHILD | WS_VISIBLE, 0, 20, 450, 20,
			hDlg, NULL, NULL, NULL);
		hStatic = CreateWindowEx(0, L"STATIC", L"Note:",
			WS_CHILD | WS_VISIBLE, 0, 40, 450, 20,
			hDlg, NULL, NULL, NULL);
		SetWindowFont(hStatic, hFont, 0);
		hNote = CreateWindowEx(0, L"EDIT", L"",
			WS_CHILD | WS_VISIBLE|ES_MULTILINE, 0, 60, 450, 170,
			hDlg, NULL, NULL, NULL);
		SetWindowFont(hTag, hFont, 0);
		SetWindowFont(hNote, hFont, 0);

		return (INT_PTR)TRUE;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_CANCEL:
							hHook = NULL;
						   EndDialog(hDlg, LOWORD(wParam));
						   return (INT_PTR)TRUE;
		case IDC_SAVE:
		{
						 //xử lý lấy chuỗi tag rồi tách ra từng tag 
						 string content = getTextToStr(hNote);
						 CNote* noteTemp= new CNote(content);
						 sNote.push_back(noteTemp);

						 string strTag = getTextToStr(hTag);
						 vector<string> SS; //Vector of string 
						 while (!strTag.empty())
						 {
							 SS.push_back(strTag.substr(0, strTag.find(",")));// add word to vector 
							 if (strTag.find(",") > strTag.size()) //Check if found " " (Space) 
							 {
								 break;
							 }
							 else
							 {
								 strTag.erase(0, strTag.find(",") + 2); // Update string 
							 }
						 }
						 for (int i = 0; i < SS.size(); i++)
						 {
							 CTag* tagTemp = new CTag;
							 if (findTag(SS[i]) == -1)
							 {
								 tagTemp->setKey(SS[i]);
								 tagTemp->addNote(noteTemp);
								 sTag.push_back(tagTemp);
							 }
							 else
							 {
								 sTag[i]->addNote(noteTemp);
							 }
						 }
						 SetWindowText(hTag, L"");
						 SetWindowText(hNote, L"");
						 MessageBox(0, L"Succession!!!", L"Notification", 0);
						 break;
		}
		}
		break;
	case WM_DESTROY:
	{
					   if (hHook == NULL) hHook = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)HookProc, hInst, 0);
					   EndDialog(hDlg, LOWORD(wParam));
					   return (INT_PTR)TRUE;
	}

	}
	return (INT_PTR)FALSE;
}

WCHAR* s2ws(const string s)
{
	int len;
	int slength = (int)s.length() + 1;
	WCHAR* buf = new WCHAR[slength];
	for (int i = 0; i < slength; i++)
	{
		buf[i] = s[i];
	}
	return buf;
}

INT_PTR CALLBACK ViewNotes(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		gListView1 = CreateWindowEx(NULL, WC_LISTVIEWW, L"",
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | WS_BORDER | LVS_EX_FULLROWSELECT,
			0, 0, 300, 250, hDlg, (HMENU)IDC_LISTVIEW1, hInst, NULL);
		// tạo cột list view
		LVCOLUMN lvCol;

		lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
		lvCol.fmt = LVCFMT_LEFT;
		lvCol.pszText = L"Tag";
		lvCol.cx = 100;
		ListView_InsertColumn(gListView1, 0, &lvCol);

		lvCol.fmt = LVCFMT_RIGHT;
		lvCol.cx = 200;
		lvCol.pszText = L"Number notes";
		ListView_InsertColumn(gListView1, 1, &lvCol);
		LV_ITEM lv;
		WCHAR* buffer;
		for (int i = 0; i < sTag.size(); i++)
		{
			lv.mask = LVIF_TEXT | LVIF_PARAM;
			lv.iItem = 0;
			lv.iSubItem = 0;
			lv.pszText = s2ws(sTag[i]->getName());
			lv.lParam = (LPARAM)s2ws(sTag[i]->getName());
			ListView_InsertItem(gListView1, &lv);
			buffer = new WCHAR[10];
			wsprintfW(buffer, L"%d", sTag[i]->getNumberNote());
			ListView_SetItemText(gListView1, 0, 1, buffer);
		}
		

		gListView2 = CreateWindowEx(NULL, WC_LISTVIEWW, L"",
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | WS_BORDER | LVS_EX_FULLROWSELECT,
			0, 260, 300, 250, hDlg, (HMENU)IDC_LISTVIEW2, hInst, NULL);
		lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
		lvCol.fmt = LVCFMT_LEFT;
		lvCol.pszText = L"Notes";
		lvCol.cx = 200;
		ListView_InsertColumn(gListView2, 0, &lvCol);
		gContentNote = CreateWindowEx(NULL, L"EDIT", L"",
			WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | WS_BORDER | LVS_EX_FULLROWSELECT,
			310, 0, 250, 500, hDlg, NULL, hInst, NULL);
		return (INT_PTR)TRUE;
	case WM_NOTIFY:
	{
					  switch (LOWORD(wParam))
					  {
					  case IDC_LISTVIEW1:
						  LPNMHDR info = (LPNMHDR)lParam;
						  WCHAR buffer[1024];
						  if (NM_DBLCLK == info->code) {
							  ListView_DeleteAllItems(gListView2);
							  int index = ListView_GetNextItem(gListView1, -1, LVNI_FOCUSED);
							  LVITEM item;

							  item.mask = LVIF_PARAM;
							  item.iSubItem = 1;
							  item.lParam = (LPARAM)buffer;
							  item.cchTextMax = 255;
							  item.iItem = index;
							  ListView_GetItem(gListView1, &item);

							  int pos = findTag((string)CW2A((LPCWSTR)item.lParam));
							  vector<CNote*> vTemp = sTag[pos]->getVNote();

							  LV_ITEM lv;
							  WCHAR* buffer;
							  for (int i = 0; i < vTemp.size(); i++)
							  {
								  lv.mask = LVIF_TEXT | LVIF_PARAM;
								  lv.iItem = 0;
								  lv.iSubItem = 0;
								  lv.pszText = s2ws(vTemp[i]->get());
								  lv.lParam = (LPARAM)s2ws(vTemp[i]->get());
								  ListView_InsertItem(gListView2, &lv);
								  buffer = new WCHAR[10];
								  wsprintfW(buffer, L"%d", sTag[i]->getNumberNote());
								  ListView_SetItemText(gListView2, 0, 1, buffer);
							  }
						  }
						  break;
					  }
					  break;
	}
		break;
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
	}
	return (INT_PTR)FALSE;
}

string getTextToStr(HWND h)
{
	WCHAR* buff;
	int len = GetWindowTextLength(h);
	buff = new WCHAR[len + 1];
	GetWindowText(h, buff, len + 1);
	wstring ws(buff);
	string str(ws.begin(), ws.end());
	delete[]buff;
	return str;
}

int findTag(string tagName)
{
	for (int i = 0; i < sTag.size(); i++)
	{
		if (sTag[i]->getName() == tagName) return i;
	}
	return -1;
}