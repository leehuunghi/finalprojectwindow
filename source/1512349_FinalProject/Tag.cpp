#include "stdafx.h"
#include "Tag.h"


CTag::CTag()
{
}


CTag::~CTag()
{
	for (int i = 0; i < sNotes.size(); i++)
	{
		delete sNotes[i];
	}
}

void CTag::addNote(CNote* note)
{
	sNotes.push_back(note);
}

void CTag::setKey(string key)
{
	this->key = key;
}